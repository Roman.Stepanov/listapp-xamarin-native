﻿using Android.App;
using Android.Views;
using Android.Widget;
using ListApp.Core;
using FFImageLoading;
using System.Collections.Generic;

namespace ListApp.Droid
{
    internal class ListViewAdapter : BaseAdapter<Beer>
    {
        private List<Beer> _items;
        private Activity _context;
        public ListViewAdapter(Activity context, List<Beer> items)
            : base()
        {
            _context = context;
            _items = items;
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override Beer this[int position]
        {
            get { return _items[position]; }
        }
        public override int Count
        {
            get { return _items.Count; }
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = _items[position];
            View view = convertView;
            if (view == null) 
                view = _context.LayoutInflater.Inflate(Resource.Layout.list_item, null);
            view.FindViewById<TextView>(Resource.Id.ListItemTextView).Text = item.Name;
            var imageView = view.FindViewById<ImageView>(Resource.Id.ListItemImageView);
            ImageService.Instance.LoadUrl(item.Image_url).Into(imageView);

            return view;
        }

        public void UpdateItems(IEnumerable<Beer> items)
        {
            if (items == null)
                return;

            _items.Clear();
            AddItems(items);
        }

        public void AddItems(IEnumerable<Beer> items)
        {
            if (items == null)
                return;

            _items.AddRange(items);
            this.NotifyDataSetChanged();
        }
    }
}