﻿using Android.App;
using Android.OS;
using Android.Views;
using System.Collections.Generic;
using ListApp.Core;
using Android.Content;
using Android.Widget;
using Toolbar = AndroidX.AppCompat.Widget.Toolbar;
using System.Linq;
using System;
using System.IO;

namespace ListApp.Droid.Activities
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : BaseActivity
    {
        internal const string ITEM_LIST_ID = "ItemListID";
        private ListViewAdapter _listViewAdapter;
        private List<Beer> _allItems = new List<Beer>();
        private const int ITEMS_PER_PAGINATION = 20;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Toolbar toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            ListView listView = FindViewById<ListView>(Resource.Id.listView1);
            _listViewAdapter = new ListViewAdapter(this, new List<Beer>());
            listView.Adapter = _listViewAdapter;
            listView.ItemClick += ListView_ItemClick;
            listView.Scroll += ListView_Scroll;

            UpdateListViewItems();
        }

        private void ListView_Scroll(object sender, AbsListView.ScrollEventArgs e)
        {
            if (e.FirstVisibleItem + e.VisibleItemCount >= e.TotalItemCount)
            {
                var newItems = _allItems.Skip(e.FirstVisibleItem + e.VisibleItemCount).Take(e.VisibleItemCount);
                _listViewAdapter.AddItems(newItems);
            }
        }

        private void ListView_ItemClick(object sender, Android.Widget.AdapterView.ItemClickEventArgs e)
        {
            var item = e.Parent.GetItemAtPosition(e.Position);
            var beer = CastJavaObject.Cast<Beer>(item);
            var intent = new Intent(this, typeof(DetailActivity));
            intent.PutExtra(ITEM_LIST_ID, beer.Id);
            StartActivity(intent);
        }

        private async void UpdateListViewItems()
        {
            SetVisibleProgressBar(true);

            try
            {
                _allItems = await DataHelper.GetBeersListAsync();
                _listViewAdapter.UpdateItems(_allItems.Take(ITEMS_PER_PAGINATION));
            }
            catch (Exception exception)
            {
                DataHelper.ErrorEventInvoke(this, new ErrorEventArgs(exception));
            }
            finally
            {
                SetVisibleProgressBar(false);
            }   
        }

        private async void UpdateListViewItemsFromServer()
        {
            SetVisibleProgressBar(true);

            try
            {
                _allItems = await DataHelper.Api.GetBeersListAsync();
                _listViewAdapter.UpdateItems(_allItems.Take(ITEMS_PER_PAGINATION));
            }
            finally
            {
                SetVisibleProgressBar(false);
            }
        }

        private void SetVisibleProgressBar(bool isVisible)
        {
            var progressBar = FindViewById<ProgressBar>(Resource.Id.ListViewProgressBar);
            ListView listView = FindViewById<ListView>(Resource.Id.listView1);

            if (isVisible)
            {
                progressBar.Visibility = ViewStates.Visible;
                listView.Visibility = ViewStates.Invisible;
            }
            else
            {
                progressBar.Visibility = ViewStates.Invisible;
                listView.Visibility = ViewStates.Visible;
            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_update)
            {
                UpdateListViewItems();
                return true;
            }

            if (id == Resource.Id.action_force_update)
            {
                UpdateListViewItemsFromServer();
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }
	}
}