﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using ListApp.Core;
using FFImageLoading;

namespace ListApp.Droid.Activities
{
    [Activity(Label = "@string/detail_activity_name")]
    public class DetailActivity : BaseActivity
    {
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_detail);

            int id = Intent.GetIntExtra(MainActivity.ITEM_LIST_ID, 0);
            var item = await DataBase.GetBeerItem(id);

            var imageView = FindViewById<ImageView>(Resource.Id.DetailImageView);
            ImageService.Instance.LoadUrl(item.Image_url).Into(imageView);

            FindViewById<TextView>(Resource.Id.DetailNameTextView).Text = item.Name;
            FindViewById<TextView>(Resource.Id.DetailFirstbrewedTextView).Text = item.First_brewed;
            FindViewById<TextView>(Resource.Id.DetailDescriptionTextView).Text = item.Description;
            FindViewById<TextView>(Resource.Id.DetailTagLineTextView).Text = item.TagLine;
        }
    }
}