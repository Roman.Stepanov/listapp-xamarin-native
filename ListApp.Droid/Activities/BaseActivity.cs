﻿using Android.App;
using Android.OS;
using Android.Widget;
using AndroidX.AppCompat.App;
using ListApp.Core;
using System.IO;
using AlertDialog = AndroidX.AppCompat.App.AlertDialog;

namespace ListApp.Droid.Activities
{
    [Activity(Label = "BaseActivity")]
    public class BaseActivity : AppCompatActivity
    {
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            DataHelper.ErrorEvent += ViewModel_ErrorEvent;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            DataHelper.ErrorEvent -= ViewModel_ErrorEvent;
        }
        private void ViewModel_ErrorEvent(object sender, ErrorEventArgs e)
        {
            var alert = new AlertDialog.Builder(this);
            alert.SetTitle("Error");
            alert.SetMessage(e.GetException().Message);
            alert.SetPositiveButton("Ok", (s, args)=> { });
            alert.Show();
        }
    }
}