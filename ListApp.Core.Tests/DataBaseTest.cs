﻿using System.Collections.Generic;
using Xunit;

namespace ListApp.Core.Tests
{

    public class DataBaseTest
    {
        [Theory]
        [InlineData(0)]
        [InlineData(int.MaxValue)]
        public async void SaveBeerItemsAsyncNullTest(int value)
        {
            await DataBase.SaveBeerItemsAsync(null);
            var result = await DataBase.GetBeerItem(value);
            Assert.Null(result); 
        }

        [Fact]
        public async void SaveBeerItemsAsyncDataTest()
        {
            var beer1 = new Beer()
            {

                Id = 76,
                Name = "#Mashtag 2013",
                Description = "A rich, nutty Brown Ale with a deep, rich malt profile, hopped with a range of citrusy and spicy US varieties. Mellow yet endlessly complex, with enough chocolate and fruit character to balance the bitterness, and a soft oaky roundness.",
                First_brewed = "05/2013",
                Image_url = "https://images.punkapi.com/v2/76.png",
                TagLine = "Democratic American Brown Ale.",
            };

            var beer2 = new Beer()
            {
                Id = 198,
                Name = "AB:01",
                Description = "Made with the yeast from a bottle of Westvleteren 12, aged with vanilla beans. Only available in 375ml bottles. Limited to 3200 bottles. The first in our Abstrakt series.",
                First_brewed = "02/2010",
                Image_url = "https://images.punkapi.com/v2/198.png",
                TagLine = "Vanilla Bean Infused Belgian Quad."

            };

            var beer3 = new Beer()
            {
                Id = 197,
                Name = "Moshi Moshi 15",
                First_brewed = "11/2013",
                Description = "A riot of C-hops, with layers of grapefruit, lime zest, pine needles, freshly cut grass, pungent resin, layered up on toasty malt with a touch of caramel sweetness.",
                Image_url = "https://images.punkapi.com/v2/197.png",
                TagLine = "American Pale Ale Birthday Beer."
            };

            var data = new List<Beer>();
            data.Add(beer1);
            data.Add(beer2);
            data.Add(beer3);

            await DataBase.SaveBeerItemsAsync(data);
            var result = await DataBase.GetBeerItem(beer3.Id);

            Assert.Equal(result.Description, beer3.Description);
        }
    }
}
