using System;
using Xunit;

namespace ListApp.Core.Tests
{
    public class RequestsTests
    {
        [Fact]
        public async void GetBeersListAsyncTest()
        {
            var requests = new Requests();
            var result = await requests.GetBeersListAsync();
            Assert.NotNull(result);
        }

        [Fact]
        public async void GetBeersListAsyncInvalidUrlTest()
        {
            DataHelper.ErrorEvent += (s, e) => 
            {
                var message = e.GetException().Message;
                System.Diagnostics.Debug.WriteLine(message);
            };
            var requests = new Requests("https://api.punkapi.com/v345");
            var result = await requests.GetBeersListAsync();
            Assert.Null(result);
        }
    }
}
