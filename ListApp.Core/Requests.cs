﻿using Refit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ListApp.Core
{
    public class Requests
    {
        private static string URL = "https://api.punkapi.com/v2";
        public IApi HttpApiService;

        public Requests(): this(URL)
        {

        }

        public Requests(string url)
        {
            var refitSettings = new RefitSettings { CollectionFormat = CollectionFormat.Multi };
            HttpApiService = RestService.For<IApi>(url, refitSettings);
        }

        public async Task<List<Beer>> GetBeersListAsync()
        {
            var beerList = new List<Beer>();
            try
            {
                var requestTasks = new List<Task<Beer[]>>();

                for (int page = 1; page <=10; page++)
                {
                    var task = HttpApiService.GetBeersList(page, 80);
                    requestTasks.Add(task);
                }

                await Task.WhenAll(requestTasks);

                if (requestTasks.All(request => !request.IsFaulted))
                {
                    foreach (Task<Beer[]> task in requestTasks)
                    {
                        beerList.AddRange(task.Result);
                    }
                }

                if (beerList.Count > 0)
                    await DataBase.SaveBeerItemsAsync(beerList);

                return beerList.OrderBy(x => x.Name).ToList();
            }
            catch (Exception exception)
            {
                DataHelper.ErrorEventInvoke(null, new ErrorEventArgs(exception));
            }

            return null;
        }
    }
}