﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ListApp.Core
{
    public static class DataBase
    {
        private const string _databaseFilename = "ItemsDataBase.db3";

        private const SQLiteOpenFlags Flags =
            // open the database in read/write mode
            SQLite.SQLiteOpenFlags.ReadWrite |
            // create the database if it doesn't exist
            SQLite.SQLiteOpenFlags.Create |
            // enable multi-threaded database access
            SQLite.SQLiteOpenFlags.SharedCache;

        private static readonly Lazy<SQLiteAsyncConnection> _lazyInitializer = new Lazy<SQLiteAsyncConnection>(() =>
        {
            var basePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            string databasePath = Path.Combine(basePath, _databaseFilename);
            return new SQLiteAsyncConnection(databasePath, Flags);
        });
        private static SQLiteAsyncConnection _database => _lazyInitializer.Value;
        private static bool _initialized = false;

        static DataBase()
        {
            _ = InitializeAsync();
        }

        private static async Task InitializeAsync()
        {
            try
            {
                if (!_initialized)
                {
                    await _database.CreateTablesAsync(CreateFlags.None, typeof(Beer)).ConfigureAwait(false);
                    _initialized = true;
                }
            }
            catch (Exception exception)
            {
                DataHelper.ErrorEventInvoke(null, new ErrorEventArgs(exception));
            }
        }

        public static Task<List<Beer>> GetBeersList()
        {
            return _database.Table<Beer>().OrderBy(x => x.Name).ToListAsync();
        }

        public static Task<Beer> GetBeerItem(int id)
        {
            return _database.Table<Beer>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public static async Task SaveBeerItemsAsync(IEnumerable<Beer> beerItems)
        {
            if (beerItems == null)
                return;

            await _database.CreateTableAsync<Beer>();
            await _database.DeleteAllAsync<Beer>();
            await _database.InsertAllAsync(beerItems);
        }
    }
}
