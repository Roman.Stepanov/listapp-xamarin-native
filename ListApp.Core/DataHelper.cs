﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace ListApp.Core
{
    public static class DataHelper
    {
        public static readonly Requests Api = new Requests();
        public static event EventHandler<ErrorEventArgs> ErrorEvent;
        public static async Task<List<Beer>> GetBeersListAsync()
        {
            List<Beer> beersList = await DataBase.GetBeersList();
            
            if (beersList?.Count == 0)
                beersList = await Api.GetBeersListAsync();

            return beersList;
        }

        public static void ErrorEventInvoke(object sender, ErrorEventArgs e)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine(e.ToString());
#endif
            ErrorEvent.Invoke(sender, e);
        }
    }
}
