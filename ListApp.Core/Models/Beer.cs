﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace ListApp.Core
{
    //[Table("Items")]
    public class Beer
    {
        [PrimaryKey]
        public int Id { get; set; }
        public string Name { get; set; }
        public string TagLine { get; set; }
        public string Description { get; set; }
        public string Image_url { get; set; }
        public string First_brewed { get; set; }
    }
}