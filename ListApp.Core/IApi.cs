﻿using Refit;
using System.Threading.Tasks;

namespace ListApp.Core
{
    public interface IApi
    {
        [Get("/beers")]
        Task<Beer[]> GetBeersList(int page, int per_page);
    }
}
